import React, { Component } from "react";
import ItemGlasses from "./ItemGlasses";

export default class extends Component {
  renderGlasses = () => {
    return this.props.list.map((glasses) => {
      return (
        <ItemGlasses
          handleChangeOverlayItem={this.props.handleChangeGlasses}
          data={glasses}
          key={glasses.id}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderGlasses()}</div>;
  }
}
