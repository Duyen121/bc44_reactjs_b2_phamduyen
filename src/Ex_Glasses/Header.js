import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <div className="header pt-0">
        <h4 className="title p-4">TRY GLASSES APP ONLINE</h4>
      </div>
    );
  }
}
