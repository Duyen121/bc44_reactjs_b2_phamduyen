import React, { Component } from "react";
import { glassesArr } from "./data";
import Header from "./Header";
import Model from "./Model";
import ListGlasses from "./ListGlasses";
import OverlayItem from "./OverlayItem";

export default class Ex_Glasses extends Component {
  state = {
    glassesArr: glassesArr,
    overlayItem: glassesArr[0],
  };

  handleChangeGlasses = (glasses) => {
    this.setState({ overlayItem: glasses });
  };

  render() {
    return (
      <div className="content">
        <Header />
        <div className="model-section">
          <div className="model-left">
            <Model />
          </div>
          <div className="model-right">
            <Model />
            <OverlayItem item={this.state.overlayItem} />
          </div>
        </div>
        <div className="glasses-section">
          <ListGlasses
            handleChangeGlasses={this.handleChangeGlasses}
            list={this.state.glassesArr}
          />
        </div>
      </div>
    );
  }
}
