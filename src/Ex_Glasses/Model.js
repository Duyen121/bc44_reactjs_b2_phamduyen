import React, { Component } from "react";

export default class Model extends Component {
  render() {
    return (
      <div className="model-items">
        <img src="./glassesImg/model.jpg" alt="modelImg" />
      </div>
    );
  }
}
