import React, { Component } from "react";

export default class OverlayItem extends Component {
  render() {
    let { url, name, desc } = this.props.item;
    return (
      <div className="overlay-items">
        <img src={url} alt={name} />
        <div className="item-detail text-left">
          <div className="item-content">
            <div className="item-title">{name}</div>
            <div className="item-desc">{desc}</div>
          </div>
        </div>
      </div>
    );
  }
}
