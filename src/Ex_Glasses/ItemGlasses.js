import React, { Component } from "react";

export default class ItemGlasses extends Component {
  render() {
    let { data, handleChangeOverlayItem } = this.props;
    let { name, url } = data;
    return (
      <div className="glasses-item col-2">
        <img
          onClick={() => {
            handleChangeOverlayItem(data);
          }}
          src={url}
          alt={name}
        />
      </div>
    );
  }
}
